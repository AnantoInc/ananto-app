package app.ananto.ananto.MainScreen.fieldSelectionScreen

import android.content.Intent
import android.os.Bundle
import app.ananto.ananto.MainScreen.MainActivity
import app.ananto.ananto.R
import app.ananto.ananto.Shared.BaseActivity
import kotlinx.android.synthetic.main.activity_field_select.*

class FieldSelectActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_field_select)

        start_btn.setOnClickListener{
            val intent = Intent(this,MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }



    // think simple you will do it, you just need to get started.
}
