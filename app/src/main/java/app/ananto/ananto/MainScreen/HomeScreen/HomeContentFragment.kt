package app.ananto.ananto.MainScreen.HomeScreen

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import app.ananto.ananto.R
import app.ananto.ananto.MainScreen.HomeScreen.dummy.DummyContent

class HomeContentFragment : Fragment() {


    private val model: HomeViewModel by lazy {
        ViewModelProviders.of(this).get(HomeViewModel::class.java)
    }


    private var mColumnCount = 1
    private var mListener: OnContentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("Callback_Home","onCreate")
        if (arguments != null) {
            mColumnCount = arguments!!.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_homecontent_list, container, false)
        Log.d("Callback_Home","OnCreateView")
        // Set the adapter
        val rv: RecyclerView = view.findViewById(R.id.content_list)
        val context = view.context
        Log.d("Context Name",context.toString())

        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = MyHomeContentRecyclerViewAdapter(DummyContent.ITEMS, mListener)

        return view
    }


    override fun onDetach() {
        super.onDetach()
        mListener = null
        Log.d("Callback_Home","onDetach")
    }

    override fun onResume() {
        super.onResume()

        Log.d("Callback_Home","onResume")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("Callback_Home","onDestroy")
    }

    companion object {

        // TODO: Customize parameter argument names
        private val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        fun newInstance(columnCount: Int): HomeContentFragment {
            val fragment = HomeContentFragment()
            val args = Bundle()
            args.putInt(ARG_COLUMN_COUNT, columnCount)
            fragment.arguments = args
            return fragment
        }
    }
}
