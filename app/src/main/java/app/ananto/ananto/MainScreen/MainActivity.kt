package app.ananto.ananto.MainScreen

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.util.Log
import android.view.Menu
import app.ananto.ananto.MainScreen.ExploreScreen.TopicFragment
import app.ananto.ananto.MainScreen.HomeScreen.HomeContentFragment
import app.ananto.ananto.R
import app.ananto.ananto.Shared.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), TopicFragment.OnListFragmentInteractionListener {


    private val homeFragment = HomeContentFragment.newInstance(100)
    private val discoverFragment = TopicFragment.newInstance(100)


    override fun onListFragmentInteraction(item: app.ananto.ananto.MainScreen.ExploreScreen.dummy.DummyContent.DummyItem) {
        Log.d("output",item.toString())
    }


    private val model: MainViewModel by lazy {
        ViewModelProviders.of(this).get(MainViewModel::class.java)
    }

    private val tabChangeObserver = Observer<Int> {
        currentTab -> loadTab(currentTab)
        Log.d("Tab Update",currentTab.toString())
    }

    private fun loadTab(tabId: Int?) {

        when(tabId) {
            R.id.navigation_home -> replaceFragment(homeFragment)
            R.id.navigation_explore -> replaceFragment(discoverFragment)
        }
    }

    private fun replaceFragment(fragment : Fragment) {

        supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container,fragment)
                .commit()

    }


    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                model.newlySelectedTab(R.id.navigation_home)
                return@OnNavigationItemSelectedListener true
            }

            R.id.navigation_explore -> {
               model.newlySelectedTab(R.id.navigation_explore)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_you -> {
               model.newlySelectedTab(R.id.navigation_you)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.top_toolbar))


        // bottom navigation listener
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        model.getCurrentTab().observe(this,tabChangeObserver)

    }

    fun switchToDefaultTab() {
        model.newlySelectedTab(R.id.navigation_home)
        navigation.selectedItemId = R.id.navigation_home
    }

    override fun onBackPressed() {

        if(model.getCurrentTab().value != R.id.navigation_home)
            switchToDefaultTab()
        else
            super.onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.top_navigation,menu)
        return true
    }


}
