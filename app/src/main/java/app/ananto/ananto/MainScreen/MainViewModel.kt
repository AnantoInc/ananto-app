package app.ananto.ananto.MainScreen

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import app.ananto.ananto.R

/**
 * Created by udit on 7/8/18.
 */

class MainViewModel : ViewModel() {

    private val currentTab: MutableLiveData<Int> = MutableLiveData()

    init {
      currentTab.value = R.id.navigation_home
    }

    fun getCurrentTab(): MutableLiveData<Int> {

        return currentTab
    }

    fun newlySelectedTab(tabId : Int) {

        if(currentTab.value!=tabId)
            currentTab.value = tabId
    }


}

