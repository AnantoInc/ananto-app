package app.ananto.ananto.MainScreen.HomeScreen

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import app.ananto.ananto.MainScreen.HomeScreen.dummy.DummyContent.DummyItem
import app.ananto.ananto.R

class MyHomeContentRecyclerViewAdapter(private val mValues:List<DummyItem>, private val mListener:OnContentInteractionListener?) : RecyclerView.Adapter<MyHomeContentRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent:ViewGroup, viewType:Int):ViewHolder {
        val view = LayoutInflater.from(parent.context)
        .inflate(R.layout.fragment_homecontent, parent, false)
        return ViewHolder(view)
    }

     override fun onBindViewHolder(holder:ViewHolder, position:Int) {
         Log.d("Items",mValues[position].toString())
         holder.mItem = mValues[position]
         holder.mIdView.text = mValues[position].id
         holder.mContentView.text = mValues[position].content

        holder.mView.setOnClickListener(object:View.OnClickListener {
            override fun onClick(v:View) {
                if (null != mListener)
                {
                    //mListener.onListFragmentInteraction(holder.mItem!!)
                }
            }
        })
    }

    override fun getItemCount():Int {
        return mValues.size
    }


    inner class ViewHolder( val mView:View):RecyclerView.ViewHolder(mView) {
        val mIdView:TextView = mView.findViewById(R.id.id) as TextView
        val mContentView:TextView = mView.findViewById(R.id.content) as TextView
        var mItem:DummyItem? = null

        init{
            Log.d("Constructor","View Holder Constructor Called")
        }

        override fun toString():String {
            return super.toString() + " '" + mContentView.getText() + "'"
        }
    }
}
