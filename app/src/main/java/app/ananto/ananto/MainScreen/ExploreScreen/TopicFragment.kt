package app.ananto.ananto.MainScreen.ExploreScreen

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import app.ananto.ananto.R
import app.ananto.ananto.MainScreen.ExploreScreen.dummy.DummyContent
import app.ananto.ananto.MainScreen.ExploreScreen.dummy.DummyContent.DummyItem

class TopicFragment : Fragment() {

    private var mColumnCount = 1
    private var mListener: OnListFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("Callback_Discover","onCreate")
        if (arguments != null) {
            mColumnCount = arguments!!.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_topic_list, container, false)
        Log.d("Callback_Discover","onCreateView")
        // Set the adapter
        if (view is RecyclerView) {
            val context = view.getContext()
            view.layoutManager = LinearLayoutManager(context)
            view.adapter = MyTopicRecyclerViewAdapter(DummyContent.ITEMS, mListener)
        }
        return view
    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        Log.d("Callback_Discover","onDetach")
        mListener = null
    }

    override fun onPause() {
        super.onPause()
        Log.d("Callback_Discover","onPause")
    }


    override fun onResume() {
        super.onResume()
        Log.d("Callback_Discover","onResume")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("Callback_Discover","onDestroy")
    }

    interface OnListFragmentInteractionListener {

        fun onListFragmentInteraction(item: DummyItem)
    }

    companion object {


        private val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        fun newInstance(columnCount: Int): TopicFragment {
            val fragment = TopicFragment()
            val args = Bundle()
            args.putInt(ARG_COLUMN_COUNT, columnCount)
            fragment.arguments = args
            return fragment
        }
    }
}
