package app.ananto.ananto.di

import android.app.Application
import android.content.Context
import android.content.res.Configuration
import android.util.Log
import app.ananto.ananto.LanguageScreen.LocaleManager
import app.ananto.ananto.LoginSignupScreen.di.onboardingModule
import org.koin.android.ext.android.startKoin

class MyApplication : Application(){

    override fun onCreate() {
        
        super.onCreate()

        startKoin(this, listOf(sharedModule, globalModule, remote_ds_module,onboardingModule), loadProperties = true)

    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(base!!))
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        LocaleManager.setLocale(this)
        Log.d("APP_IMP","onConfigChanged "+ newConfig!!.locale.language)
    }
}