package app.ananto.ananto.di

import app.ananto.ananto.FirstScreen.OpeningViewModel
import app.ananto.ananto.LanguageScreen.LanguageViewModel
import app.ananto.ananto.Shared.AppRepository
import app.ananto.ananto.Shared.AppRepositoryImpl
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.Module
import org.koin.dsl.module.module


val globalModule: Module = module {


}

val sharedModule:Module = module {

    // module help us logically organise the injections and manage their lifecycles

    module("languageChoice") {

        viewModel { LanguageViewModel(get()) }

    }

    viewModel { OpeningViewModel(get()) }

    single( definition = { AppRepositoryImpl(androidContext()) as AppRepository })



}
