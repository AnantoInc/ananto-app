package app.ananto.ananto.FirstScreen

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import app.ananto.ananto.LanguageScreen.LanguageActivity
import app.ananto.ananto.MainScreen.MainActivity
import app.ananto.ananto.R
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * A full-screen activity
 */


class FirstActivity : AppCompatActivity() {

    val openingViewModel by viewModel<OpeningViewModel>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_first)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


    }

    override fun onResume() {
        super.onResume()

        val intent = if(openingViewModel.isUserLoggedIn())
            Intent(this,MainActivity::class.java)
        else
            Intent(this,LanguageActivity::class.java)

        startActivity(intent)
        finish()

    }
}
