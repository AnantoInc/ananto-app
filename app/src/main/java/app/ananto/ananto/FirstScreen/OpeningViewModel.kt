package app.ananto.ananto.FirstScreen

import android.arch.lifecycle.ViewModel
import app.ananto.ananto.Shared.AppRepository

class OpeningViewModel(val appRepository: AppRepository) : ViewModel() {


    fun isUserLoggedIn() : Boolean {
        return appRepository.fetchLoginStatus()
    }




}