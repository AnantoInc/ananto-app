package app.ananto.ananto.LoginSignupScreen.Services

import app.ananto.ananto.LoginSignupScreen.Repository.GoogleUserCredential
import app.ananto.ananto.LoginSignupScreen.Repository.AuthDataContract
import io.reactivex.Single
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Retrofit

class RemoteApiService : AuthDataContract.RemoteApi, KoinComponent{

    val retrofit: Retrofit by inject()
    val networkService = retrofit.create(AuthNetworkService::class.java)

    override fun fetchLoginValidation(googleUserCredential: GoogleUserCredential, langChoice:String): Single<AuthRemoteDataModel.ResponseData> {

        return networkService.validateGoogleSignin(AuthRemoteDataModel.RequestData(userCredential = googleUserCredential, langChoice = langChoice))

    }

}