package app.ananto.ananto.LoginSignupScreen.Services

import app.ananto.ananto.LoginSignupScreen.Repository.GoogleUserCredential
import app.ananto.ananto.datamodel.UserData


object AuthRemoteDataModel {

    data class RequestData(val userCredential: GoogleUserCredential, val langChoice:String)


    data class ResponseData(val validationStatus:Boolean, val jwtToken:String?, val userData:UserData? )

}