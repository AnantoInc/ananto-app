package app.ananto.ananto.LoginSignupScreen.di

import app.ananto.ananto.LoginSignupScreen.LoginSignupViewModel
import app.ananto.ananto.LoginSignupScreen.Repository.AuthDataContract
import app.ananto.ananto.LoginSignupScreen.Repository.AuthRepositoryImpl
import app.ananto.ananto.LoginSignupScreen.Services.RemoteApiService
import io.reactivex.disposables.CompositeDisposable
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.Module
import org.koin.dsl.module.module

val onboardingModule:Module = module{


    module("loginSignup") {


        viewModel { LoginSignupViewModel(get(),get()) }

        single { AuthRepositoryImpl(get(), get(), get(),get()) as AuthDataContract.Repository }

        single {RemoteApiService() as AuthDataContract.RemoteApi}


        single {CompositeDisposable()}

    }

}