package app.ananto.ananto.LoginSignupScreen

import android.arch.lifecycle.Observer
import android.os.Bundle
import app.ananto.ananto.R
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.SignInButton
import kotlinx.android.synthetic.main.activity_login_signup.*
import android.content.Intent
import android.util.Log
import app.ananto.ananto.LoginSignupScreen.Repository.SignupState
import app.ananto.ananto.MainScreen.fieldSelectionScreen.FieldSelectActivity
import app.ananto.ananto.Shared.BaseActivity
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import org.koin.android.viewmodel.ext.android.viewModel


class LoginSignupActivity :BaseActivity() {


    val loginSignupViewModel: LoginSignupViewModel by viewModel()

    companion object {
        private const val RC_SIGN_IN = 100
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_signup)
        Log.d("LOGIN_ACTIVITY","RECREATED")
        buildGoogleSignInClient()
        loginSignupViewModel.signupState.observe(this, Observer { signUpState: SignupState? ->
            when(signUpState) {
                SignupState.WAITING_VALIDATION -> { onWaitingValidation() }
                SignupState.INVALID -> { onInvalidCredential()}
                SignupState.VALID -> { onValidCredential() }
                SignupState.ERROR -> { onNetworkError() }
            }
        }

        )

    }


    fun onWaitingValidation() {
        Log.d("WAIT","WAITING")
    }

    fun onNetworkError() {
        Log.d("ERROR","Error")
    }

    fun onInvalidCredential() {
        Log.d("INVALID","invalid")

    }



    fun buildGoogleSignInClient() {


        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.google_clientid))
                .requestEmail()
                .build()

        var mGoogleSignInClient = GoogleSignIn.getClient(this, gso)

        sign_in_button.setSize(SignInButton.SIZE_WIDE)

        sign_in_button.setOnClickListener{
            val signInIntent = mGoogleSignInClient.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)

        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode== RC_SIGN_IN){
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)

            handleGoogleSignInResult(task)

        }
    }

    fun handleGoogleSignInResult(completedTask: Task<GoogleSignInAccount>){
    val account:GoogleSignInAccount? = try {
        completedTask.getResult(ApiException::class.java)
    } catch(e:ApiException) {Log.w("SigninException", "signInResult:failed code=" + e.getStatusCode())
        null}
        if(account!=null)
            loginSignupViewModel.submitGoogleCredentials(account)

    }



    private fun onValidCredential(){
        //Log.d("IMP","Validated")
        val intent = Intent(this,FieldSelectActivity::class.java)
        startActivity(intent)
        finish()
    }
}
