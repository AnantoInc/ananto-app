package app.ananto.ananto.LoginSignupScreen.Services

import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthNetworkService {

    // do is more important than do it best and do it right :)

    @POST("auth/google")
    fun validateGoogleSignin(@Body data: AuthRemoteDataModel.RequestData) : Single<AuthRemoteDataModel.ResponseData>

}