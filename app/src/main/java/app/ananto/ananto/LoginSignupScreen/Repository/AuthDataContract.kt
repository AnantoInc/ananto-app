package app.ananto.ananto.LoginSignupScreen.Repository

import app.ananto.ananto.LoginSignupScreen.Services.AuthRemoteDataModel
import app.ananto.ananto.networking.Outcome
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject

interface AuthDataContract {


    interface Repository {

        val validateFetchOutcome: PublishSubject<Outcome<AuthRemoteDataModel.ResponseData>>

        fun validateGoogleLogin(googleUserCredential: GoogleUserCredential)

        fun saveUserAuthData(userAuthData: AuthRemoteDataModel.ResponseData)

    }

    interface RemoteApi {

        fun fetchLoginValidation(googleUserCredential: GoogleUserCredential,langChoice:String) : Single<AuthRemoteDataModel.ResponseData>


    }
}