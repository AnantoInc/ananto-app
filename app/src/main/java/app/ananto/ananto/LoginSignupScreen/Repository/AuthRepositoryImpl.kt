package app.ananto.ananto.LoginSignupScreen.Repository

import android.content.Context
import app.ananto.ananto.LoginSignupScreen.Services.AuthRemoteDataModel
import app.ananto.ananto.R
import app.ananto.ananto.Shared.AppRepository
import app.ananto.ananto.networking.Outcome
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject


class AuthRepositoryImpl(val remoteService:AuthDataContract.RemoteApi, val compositeDisposable: CompositeDisposable,val appContext:Context,val appRepository:AppRepository) : AuthDataContract.Repository{


    override val validateFetchOutcome: PublishSubject<Outcome<AuthRemoteDataModel.ResponseData>> = PublishSubject.create<Outcome<AuthRemoteDataModel.ResponseData>>()


    override fun validateGoogleLogin(googleUserCredential: GoogleUserCredential) {

        validateFetchOutcome.onNext(Outcome.loading(true))

        val a = remoteService.fetchLoginValidation(googleUserCredential,appRepository.fetchCurrentLanguage().name)
                .subscribeOn(Schedulers.io())
                .subscribe({ responseData: AuthRemoteDataModel.ResponseData ->
                    validateFetchOutcome.onNext(Outcome.success(responseData))
                }, { error -> validateFetchOutcome.onNext(Outcome.failure(error)) })

        compositeDisposable.add(a)

    }

    override fun saveUserAuthData(userAuthData: AuthRemoteDataModel.ResponseData) {
        val sharedPref = appContext.getSharedPreferences(appContext.getString(R.string.USER_DATA_FILE_KEY),Context.MODE_PRIVATE)
        with(sharedPref.edit()){
            putBoolean("LOGGED_IN",true)
            putString("TOKEN",userAuthData.jwtToken)
            putString("NAME", userAuthData.userData!!.name)
            if(userAuthData.userData.email!=null)
                putString("EMAIL", userAuthData.userData.email)

            if(userAuthData.userData.phoneNo!=null)
                putString("PHONE", userAuthData.userData.phoneNo)

            commit()
        }

    }

}