package app.ananto.ananto.LoginSignupScreen.Repository

data class GoogleUserCredential (

        val userDisplayName:String?,
        val firstName:String?,
        val lastName:String?,
        val photoUrl:String?,
        val email:String,
        val idToken:String,
        val googleId:String

)



