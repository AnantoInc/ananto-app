package app.ananto.ananto.LoginSignupScreen.Repository

enum class SignupState {

    WAITING_VALIDATION,
    VALID,
    INVALID,
    ERROR

}