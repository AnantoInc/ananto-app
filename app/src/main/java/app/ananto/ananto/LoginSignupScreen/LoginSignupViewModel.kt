package app.ananto.ananto.LoginSignupScreen


import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import app.ananto.ananto.LoginSignupScreen.Repository.GoogleUserCredential
import app.ananto.ananto.LoginSignupScreen.Repository.SignupState
import app.ananto.ananto.LoginSignupScreen.Repository.AuthDataContract
import app.ananto.ananto.LoginSignupScreen.Services.AuthRemoteDataModel
import app.ananto.ananto.networking.Outcome
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable


class LoginSignupViewModel(val repository: AuthDataContract.Repository, private val compositeDisposable: CompositeDisposable) : ViewModel(){

    val signupState:MutableLiveData<SignupState> = MutableLiveData()


    init {
        Log.d("LOGIN_ViewModel","RECREATED")
      val disposable = repository.validateFetchOutcome
              .observeOn(AndroidSchedulers.mainThread())
                .subscribe { outcome:Outcome<AuthRemoteDataModel.ResponseData> ->

            when(outcome){
                is Outcome.Success -> {
                    Log.d("SUCeSS","new data")
                    if(outcome.data.validationStatus){
                        Log.d("Actual Response",outcome.data.userData.toString())
                        repository.saveUserAuthData(outcome.data)
                        signupState.value = SignupState.VALID
                    }

                    else
                        signupState.value = SignupState.INVALID

                }
                is Outcome.Failure -> {
                    signupState.value = SignupState.ERROR
                }

                is Outcome.Progress -> {
                    signupState.value = SignupState.WAITING_VALIDATION
                }

            }
        }

        compositeDisposable.add(disposable)
    }



    fun submitGoogleCredentials(account:GoogleSignInAccount) {

        val googleUserCredential = GoogleUserCredential(account.displayName, account.givenName, account.familyName, account.photoUrl.toString(), account.email!!, account.idToken!!, account.id!!)

        repository.validateGoogleLogin(googleUserCredential)

    }

     override fun onCleared() {
        super.onCleared()
        Log.d("LOGIN_ViewModel","DESTROYED")
        compositeDisposable.clear()
    }





}