package app.ananto.ananto.LanguageScreen

import android.arch.lifecycle.ViewModel
import android.util.Log
import app.ananto.ananto.Shared.AppRepository

/**
 * Created by udit on 29/7/18.
 */
class LanguageViewModel (val repository: AppRepository) : ViewModel()  {



    fun getCurrentLanguage() : AvlLang {
        Log.d("Language_Test",repository.fetchCurrentLanguage().name)
        return repository.fetchCurrentLanguage()
    }

    fun setLanguage(lang : AvlLang): Unit {
        Log.d("FinalChoice",lang.name)
        repository.changeCurrentLanguage(lang)

        // Log.d("Language_change", repository.fetchCurrentLanguage().name)
    }




}