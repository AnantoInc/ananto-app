package app.ananto.ananto.LanguageScreen

import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.util.Log
import app.ananto.ananto.R
import java.util.*

class LocaleManager {

    companion object {


        fun updateResources(context: Context, lang:AvlLang): Context {

            val lang_code = when(lang) {
                AvlLang.ENGLISH -> "en"
                AvlLang.HINDI -> "hi"
            }

            var newContext = context
            lateinit var locale:Locale
            if(lang_code=="hi")
                locale = Locale(lang_code,"IN")
            else
                locale = Locale(lang_code)
            Locale.setDefault(locale)
            val res = context.resources

            val config = Configuration(res.configuration)
            if (Build.VERSION.SDK_INT >= 17) {
                config.setLocale(locale)
                newContext = context.createConfigurationContext(config)
            } else {
                config.locale = locale
                res.updateConfiguration(config,res.displayMetrics)
            }

            return newContext

        }

        fun getLanguage(context:Context): AvlLang {
            val sharedPref = context.getSharedPreferences(context.getString(R.string.USER_DATA_FILE_KEY),Context.MODE_PRIVATE)

            val choice = sharedPref.getString("LANG_CHOICE","ENGLISH")

            Log.d("LOCALE_MANAGER",choice)
            return AvlLang.valueOf(choice)
        }


        fun setLocale(c:Context) : Context{
          return updateResources(c, getLanguage(c))

        }
    }
}