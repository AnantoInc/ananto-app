package app.ananto.ananto.LanguageScreen

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.RadioGroup
import app.ananto.ananto.LoginSignupScreen.LoginSignupActivity
import app.ananto.ananto.R
import app.ananto.ananto.Shared.BaseActivity
import kotlinx.android.synthetic.main.activity_language.*
import org.koin.android.viewmodel.ext.android.viewModel
import kotlin.collections.HashMap

class LanguageActivity : BaseActivity() {

    val languageViewModel by viewModel<LanguageViewModel>()

    val langBtn : HashMap<AvlLang,Int> = HashMap<AvlLang , Int>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_language)

        // prepare the map
        langBtn.put(AvlLang.ENGLISH , R.id.eng_btn)
        langBtn.put(AvlLang.HINDI , R.id.hindi_btn)

        lang_radio_group.check(langBtn[languageViewModel.getCurrentLanguage()]!!)

        lang_radio_group.setOnCheckedChangeListener { group:RadioGroup, checkedId:Int ->

            Log.d("RadioChoice",""+checkedId)
            languageViewModel.setLanguage(langBtn.filter {it.value == checkedId }.keys.first())
        }

        next_btn.setOnClickListener {
            val intent = Intent(this,LoginSignupActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            System.exit(0)
        }

    }
}
