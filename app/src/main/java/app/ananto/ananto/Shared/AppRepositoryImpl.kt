package app.ananto.ananto.Shared

import android.content.Context
import android.util.Log
import app.ananto.ananto.LanguageScreen.AvlLang
import app.ananto.ananto.R

class AppRepositoryImpl(val appContext:Context) : AppRepository{


    var langChoice:AvlLang

    init {

        val sharedPref = appContext.getSharedPreferences(appContext.getString(R.string.USER_DATA_FILE_KEY),Context.MODE_PRIVATE)

        val choice = sharedPref.getString("LANG_CHOICE","ENGLISH")

        langChoice = AvlLang.valueOf(choice)

    }


    override fun changeCurrentLanguage(lang: AvlLang) {


        langChoice = lang

        val sharedPref = appContext.getSharedPreferences(appContext.getString(R.string.USER_DATA_FILE_KEY),Context.MODE_PRIVATE)

        with(sharedPref.edit()) {
            putString("LANG_CHOICE",lang.name)
            apply()
        }

    }

    override fun fetchCurrentLanguage(): AvlLang {

        Log.d("Test123",appContext.packageName)

        return langChoice

    }


    override fun fetchLoginStatus(): Boolean {

        val sharedPref = appContext.getSharedPreferences(appContext.getString(R.string.USER_DATA_FILE_KEY),Context.MODE_PRIVATE)

        return sharedPref.getBoolean("LOGGED_IN",false)

    }


}