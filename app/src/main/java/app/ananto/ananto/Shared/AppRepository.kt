package app.ananto.ananto.Shared

import app.ananto.ananto.LanguageScreen.AvlLang

interface AppRepository {

    fun fetchLoginStatus():Boolean

    fun fetchCurrentLanguage(): AvlLang

    fun changeCurrentLanguage(lang: AvlLang)

}