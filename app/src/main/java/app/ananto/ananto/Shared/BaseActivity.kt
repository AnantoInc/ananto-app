package app.ananto.ananto.Shared

import android.content.Context
import android.support.v7.app.AppCompatActivity
import app.ananto.ananto.LanguageScreen.LocaleManager

open class BaseActivity : AppCompatActivity() {

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase!!))
    }

}